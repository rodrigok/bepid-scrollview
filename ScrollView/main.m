//
//  main.m
//  ScrollView
//
//  Created by Rodrigo Nascimento on 20/01/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SVAppDelegate class]));
    }
}
