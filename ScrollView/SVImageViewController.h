//
//  SVImageViewController.h
//  ScrollView
//
//  Created by Rodrigo Nascimento on 20/01/14.
//  Copyright (c) 2014 Rodrigo Nascimento. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVImageViewController : UIViewController <UIScrollViewDelegate>

@property NSInteger imageIndex;

@end
