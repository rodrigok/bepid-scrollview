//
//  SVViewController.m
//  ScrollView
//
//  Created by Rodrigo Nascimento on 20/01/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "SVViewController.h"
#import "SVImageViewController.h"

@interface SVViewController () {
    NSInteger rowIndex;
    CGFloat scale;
    CGFloat limitHeight;
}

@end

@implementation SVViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *deviceType = [UIDevice currentDevice].model;

    NSInteger height = 100;
    
    if([deviceType isEqualToString:@"iPad"])
    {
        height = 400;
    }
    
    if (limitHeight < height) {
        limitHeight = height;
    }
    
    if (indexPath.row == rowIndex) {
        height = height * scale;
    }

    if (height > limitHeight) {
        height = limitHeight;
    }

    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];

    
    UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"%d.jpg", indexPath.row]];
    
    NSNumber *height = [NSNumber numberWithFloat:tableView.frame.size.width /image.size.width * image.size.height];
    
    CGRect imageFrame = CGRectMake(0, - height.floatValue / 2 + cell.frame.size.height / 2, tableView.frame.size.width, height.floatValue);
    
    UIImageView *ImageView = (UIImageView *) [cell viewWithTag:100];
    
    if (ImageView != nil) {
        [ImageView removeFromSuperview];
    }

    ImageView = [[UIImageView alloc] initWithFrame:imageFrame];
    ImageView.tag = 100;
    
    UIPinchGestureRecognizer *twoFingerPinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(twoFingerPinch:)];
    [twoFingerPinch setDelegate:cell];

    [cell addGestureRecognizer:twoFingerPinch];
    
    [cell addSubview:ImageView];
    
    [ImageView setImage:image];
    
    return cell;
}

- (void)twoFingerPinch:(UIPinchGestureRecognizer *)recognizer {
    UITableViewCell *cell = (UITableViewCell *) recognizer.delegate;
    
    rowIndex = [[self.tableView indexPathForCell: cell] row];
    limitHeight = ((UIImageView *) [cell viewWithTag:100]).frame.size.height;
    scale = (recognizer.scale - 1) / 4 + 1;
    
    if ([recognizer state] == UIGestureRecognizerStateEnded) {
        scale = 1;
    }

    [self.tableView reloadData];
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SVImageViewController *view = (SVImageViewController *) segue.destinationViewController;
    

    view.navigationItem.title = [NSString stringWithFormat:@"Photo %d", [[self.tableView indexPathForSelectedRow] row]];
    
    view.imageIndex = [[self.tableView indexPathForSelectedRow] row];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
