//
//  SVViewController.h
//  ScrollView
//
//  Created by Rodrigo Nascimento on 20/01/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@end