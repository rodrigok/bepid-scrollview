//
//  SVImageViewController.m
//  ScrollView
//
//  Created by Rodrigo Nascimento on 20/01/14.
//  Copyright (c) 2014 Rodrigo Nascimento. All rights reserved.
//

#import "SVImageViewController.h"

@interface SVImageViewController () {
    UIImageView *ImageView;
}

@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet UISlider *Slider;
@property (weak, nonatomic) IBOutlet UISwitch *EnableSwitch;

@end

@implementation SVImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    
    [self.ScrollView addGestureRecognizer:doubleTap];
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    
    if(self.ScrollView.zoomScale > self.ScrollView.minimumZoomScale)
        [self.ScrollView setZoomScale:self.ScrollView.minimumZoomScale animated:YES];
    else
        [self.ScrollView setZoomScale:self.ScrollView.maximumZoomScale animated:YES];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (ImageView == nil) {
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"%d.jpg", self.imageIndex]];
        
        CGRect imageFrame = CGRectMake(0, 0, image.size.width, image.size.height);
        
        ImageView = [[UIImageView alloc] initWithFrame:imageFrame];
        
        [ImageView setImage:image];
        
        [self.ScrollView addSubview:ImageView];
        
        [self.ScrollView setContentSize:imageFrame.size];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    if (!self.EnableSwitch.on) {
        return nil;
    }
    
    return ImageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    [self.Slider setValue:scrollView.zoomScale animated:YES];
}

- (IBAction)sliderValueChange:(id)sender {
    self.ScrollView.zoomScale = self.Slider.value;
}

- (IBAction)switchValueChange:(id)sender {
    self.Slider.enabled = self.EnableSwitch.on;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
